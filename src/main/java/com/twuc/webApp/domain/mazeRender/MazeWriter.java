package com.twuc.webApp.domain.mazeRender;

import com.twuc.webApp.domain.mazeGenerator.Grid;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * 生成迷宫并将迷宫图片写入到 {@link OutputStream} 中。
 */
public abstract class MazeWriter {
    protected final List<AreaRender> backgroundRenders;
    protected final List<CellRender> groundRenders;
    protected final List<CellRender> wallRenders;
    protected final MazeRenderSettings settings;

    /**
     * 创建迷宫生成器。
     *
     * @param factory 该迷宫组件的工厂。
     */
    protected MazeWriter(MazeComponentFactory factory) {
        this.backgroundRenders = factory.createBackgroundRenders();
        this.groundRenders = factory.createGroundRenders();
        this.wallRenders = factory.createWallRenders();
        this.settings = factory.createSettings();
    }

    /**
     * 生成迷宫并将迷宫输出到 {@link OutputStream} 中。
     *
     * @param grid 迷宫道路网格。
     * @param stream 输出流。
     * @throws IOException 在输出过程中出现错误。但是具体什么错误我也不知道 ㄟ( ▔, ▔ )ㄏ
     */
    public abstract void render(Grid grid, OutputStream stream) throws IOException;

    /**
     * 获得当前迷宫的名称。
     *
     * @return 当前迷宫的名称。
     */
    public abstract String getName();
}
